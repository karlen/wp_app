var chokidar = require('chokidar');
var path = require('path');
var fs = require('fs');
var http = require('http');
var plugins = path.resolve(__dirname, 'plugins/*');
var components = path.resolve(__dirname, 'components/*');

var PluginWatcher = chokidar.watch(plugins, {});
var ComponentWatcher = chokidar.watch(components, {});

PluginWatcher.on('addDir', function(path) {

    var PATH =  path.split("/");
    var plugin = PATH[PATH.length -1];

    var newPackage = path+'/package.json';
    var newPlugin = path+'/'+plugin+'.js';

    if (!fileExists(newPackage)) {
        var packageJson = {

            "name": plugin,
            "version": "0.0.0",
            "description": plugin + " plugin",
            "main": plugin + ".js",

            "author": process.env.LOGNAME,

            "license": "ISC"

        };
        fs.writeFile(newPackage, JSON.stringify(packageJson, null, "\t"), function (err) {

        });
    }

    if (!fileExists(newPlugin)) {

        var mainJS = "import  React from 'react'\n\
import  ReactDOM from 'react-dom'\n\
\n\
class "+plugin+" extends React.Component {\n\
\n\
    constructor(props){\n\
        super(props);\n\
    }\n\
\n\
    render() {\n\
\n\
        return (\n\
            <div>"+plugin+" plugin</div>\n\
        );\n\
               \n\
    }\n\
\n\
}\n\
\n\
export default "+plugin+";";


        fs.writeFile(newPlugin, mainJS, function (err) {

        });
    }


});

ComponentWatcher.on('addDir', function(path) {

    var PATH =  path.split("/");
    var component = PATH[PATH.length -1];

    var newPackage = path+'/package.json';
    var newComponent = path+'/'+component+'.js';
    var newStyle = path+'/'+component+'.scss';

    if (!fileExists(newPackage)) {
        var packageJson = {

            "name": component,
            "version": "0.0.0",
            "description": component + " component",
            "main": component + ".js",

            "author": process.env.LOGNAME,

            "license": "ISC"

        };
        fs.writeFile(newPackage, JSON.stringify(packageJson, null, "\t"), function (err) {

        });
    }

    if (!fileExists(newComponent)) {

        var mainJS = "import  React from 'react'\n\
import  ReactDOM from 'react-dom'\n\
import  styles from './"+component+".scss'\n\
\n\
class "+component+" extends React.Component {\n\
\n\
    constructor(props){\n\
        super(props);\n\
    }\n\
\n\
    render() {\n\
\n\
        return (\n\
            <div>"+component+" component</div>\n\
        );\n\
\n\
    }\n\
\n\
}\n\
\n\
export default "+component+";";


        fs.writeFile(newComponent, mainJS, function (err) {

        });
    }

    if (!fileExists(newStyle)) {

        var mainSCSS = "";
        fs.writeFile(newStyle, mainSCSS, function (err) {

        });
    }


});


try{
    http.createServer(function (req, res) {
        if(fileExists(__dirname + req.url)){
            var file = __dirname + req.url;
        }else {
            var file = __dirname + '/app.html';
        }
        fs.readFile(file, function (err,data) {
            if (err) {
                res.writeHead(404);
                res.end(JSON.stringify(err));
                return;
            }
            res.writeHead(200);
            res.end(data);
        });
    }).listen(7070);
    console.log('http://localhost:7070');
}catch(e){

}

function fileExists(filePath){
    try
    {
        return fs.statSync(filePath).isFile();
    }
    catch (err)
    {
        return false;
    }
}



