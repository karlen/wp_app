'use strict';

var webpack = require('webpack');
var path = require('path');
require('es6-promise').polyfill();

module.exports = {
    entry: ["./components/App"],
    output: {
        filename: "./app.min.js",
    },

    module: {
        loaders:[{
            test: /\.jsx?$/,
            loader: 'babel',
            query: {
                presets: ["react", "es2015"]
            }
        },
            { test: /\.css$/, loader: "style-loader!css-loader" },
            { test: /\.scss$/, loaders: ["style", "css", "sass"]},
            { test: /\.png$/, loader: "url-loader?limit=100000" },
            { test: /\.svg/, loader: "url-loader?limit=100000" },
            { test: /\.jpg$/, loader: "url-loader?limit=100000" },
            { test: /\.gif/, loader: "url-loader?limit=100000" },
            { test: /\.woff/, loader: "url-loader?limit=100000" }
        ]
    },

    resolve: {
        root: path.resolve('./')
    },

    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: false

            }
        })
    ]

};

