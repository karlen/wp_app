import  React from 'react'
import  ReactDOM from 'react-dom'
import  styles from './Main.scss'

class Main extends React.Component {

    constructor(props){
        super(props);
    }

    render() {

        return (
            <div>Main component</div>
        );

    }

}

export default Main;