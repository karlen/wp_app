import  React from 'react'
import  ReactDOM from 'react-dom'
import  Login from 'components/Login'
import  Main from 'components/Main'
import  User from 'plugins/User'
import  styles from './App.scss'
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin();


class Component extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            component: <Login />
        };
    }

    render() {

        return (
            this.state.component
        );

    }

}

var app = document.createElement('div');
app.id = 'app';
document.body.appendChild(app);
ReactDOM.render(<Component />, app);

